<?php
/**
 * @file
 * Contains configuration options for the Xplanation translator.
 *
 * Options available during the translator setup are:
 *  - Webservice environment server (invisible outside the Xplanation domain)
 *  - Process Code (Provided by Xplanation)
 *  - Translation service (invisible outside our domain)
 *  - Domaincode (for example, MARKETING)
 *  - Username for the webservice user
 *  - Password for the webservice user
 *
 * Options available during a translation request are:
 *  - Due date
 *  - Project comments
 */

/**
 * Controller for the Xplanation settings interface.
 */
class TMGMTXplanationTranslatorUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * {@inheritdoc}
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {
    global $base_url;
    $dev_server = strpos($base_url, 'www-dev.xplanation.com') !== FALSE;
    $tst_server = strpos($base_url, 'www-tst.xplanation.com') !== FALSE;
    $prd_server = $dev_server || $tst_server;
    $form['server'] = array(
      '#type' => 'textfield',
      '#title' => t('Server'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('server'),
      '#description' => t('The password to request the translation with'),
      '#visible' => !$prd_server,
    );

    $process_code_desc = t('This process code will be provided by Xplanation');
    $form['process_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Process code'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('process_code'),
      '#description' => $process_code_desc,
    );
    $services = array();
    $services['TRANS'] = t('Translation');
    if (!$prd_server) {
      $services['TR-REV'] = t('Translation without revision');
    }
    $form['service'] = array(
      '#type' => 'select',
      '#title' => t('Service'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('service'),
      '#description' => t('The password to request the translation with'),
      '#options' => $services,
      '#visible' => !$prd_server,
    );
    $dom_code_desc = t('This is the domain that used for the project request.');
    $form['customer_domain_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Customer domain code'),
      '#required' => FALSE,
      '#default_value' => $translator->getSetting('customer_domain_code'),
      '#description' => $dom_code_desc,
    );
    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('username'),
      '#description' => t('The username to request the translation with'),
    );
    $form['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('password'),
      '#description' => t('The password to request the translation with'),
    );
    $form['proxy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Proxy'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['proxy']['host'] = array(
      '#type' => 'textfield',
      '#title' => t('Host'),
      '#required' => FALSE,
      '#default_value' => $translator->getSetting('proxy_host'),
      '#description' => t('The proxy host'),
    );
    $form['proxy']['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#required' => FALSE,
      '#default_value' => $translator->getSetting('proxy_port'),
      '#description' => t('The proxy port'),
    );
    $form['proxy']['login'] = array(
      '#type' => 'textfield',
      '#title' => t('Login'),
      '#required' => FALSE,
      '#default_value' => $translator->getSetting('proxy_login'),
      '#description' => t('The proxy login'),
    );
    $form['proxy']['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#required' => FALSE,
      '#default_value' => $translator->getSetting('proxy_password'),
      '#description' => t('The proxy password'),
    );
    return parent::pluginSettingsForm($form, $form_state, $translator, $busy);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm($form, &$form_state, TMGMTJob $job) {
    $format = 'Y-m-d H:i';
    $date_desc = t('By when would you like the translations to be delivered');
    $comm_desc = t('Additional information related to the translation request');
    $form['due_date'] = array(
      '#type' => 'date_select',
      '#title' => t('Due date'),
      '#description' => $date_desc,
      '#required' => TRUE,
      '#default_value' => variable_get('tmgmt_xplanation_due_date', ''),
      '#date_format' => $format,
      '#date_year_range' => '-0:+3',
      '#datepicker_options' => array(),
      '#element_validate' => array('tmgmt_xplanation_after_current_date'),
    );
    $form['project_comments'] = array(
      '#type' => 'textarea',
      '#title' => t('Comments'),
      '#default_value' => '',
      '#description' => $comm_desc,
      '#value' => variable_get('tmgmt_xplanation_comments', ''),
    );

    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(TMGMTJob $job) {
    $form["manual"] = array(
      '#type' => 'fieldset',
      '#title' => t('Manual import translated file'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    );

    if ($job->isFinished()) {
      $form["manual"]['label'] = array(
        '#type' => 'markup',
        '#markup' => t('This will also changes the job status to "Active"'),
      );
    }

    $form["manual"]['file'] = array(
      '#type' => 'file',
      '#title' => t('File file'),
      '#size' => 50,
      '#description' => t('Only supported file formats is Xplanation TMGMT XML format'),
    );
    $form["manual"]['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Import'),
      '#submit' => array('tmgmt_xplanation_file_import_form_submit'),
    );

    if ($job->isFinished()) {
      $form["status"] = array(
        '#type' => 'fieldset',
        '#title' => t('Active again'),
        '#collapsed' => TRUE,
        '#collapsible' => TRUE,
      );

      $form["status"]['label'] = array(
        '#type' => 'markup',
        '#markup' => t('This will re-activate the job and the translated file will be re-downloaded') . '<br />',
      );

      $form["status"]['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Re-Active'),
        '#submit' => array('tmgmt_xplanation_reactivate_form_submit'),
      );
    }
    return $this->checkoutInfoWrapper($job, $form);
  }

}
