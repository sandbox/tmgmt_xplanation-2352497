<?php
/**
 * @file
 * Main module file for the Xplanation translator module.
 */

/**
 * The Xplanation support mail address in html-tags.
 */
if (!defined('TMGMT_XPLANATION_SUPPORT')) {
  $support_mail = '<a href="mailto:support@xplanation.com">';
  $support_mail .= 'Xplanation support';
  $support_mail .= '</a>';
  define('TMGMT_XPLANATION_SUPPORT', $support_mail);
}

/**
 * Implements hook_tmgmt_translator_plugin_info().
 */
function tmgmt_xplanation_tmgmt_translator_plugin_info() {
  return array(
    'xplanation' => array(
      'label' => t('Xplanation'),
      'description' => t('Translate using Xplanation services.'),
      'plugin controller class' => 'TMGMTXplanationPluginController',
      'ui controller class' => 'TMGMTXplanationTranslatorUIController',
    ),
  );
}

/**
 * Implements hook_help().
 */
function tmgmt_xplanation_help($path, $arg) {
  switch ($path) {
    case 'admin/help#tmgmt_xplanation':
      $path = dirname(__FILE__) . '/README.md';
      if (file_exists($path)) {
        $readme = file_get_contents($path);
      }
      else {
        $path = dirname(__FILE__) . '/README.txt';
        if (file_exists($path)) {
          $readme = file_get_contents($path);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];
        if (function_exists($info['process callback'])) {
          $function = $info['process callback'];
          $output = filter_xss_admin($function($readme, NULL));
        }
        else {
          $output = '<pre>' . check_plain($readme) . '</pre>';
        }
      }
      else {
        $output = '<pre>' . check_plain($readme) . '</pre>';
      }
      return $output;
  }
}

/**
 * Implements callback_element_validate().
 *
 * Validates a given element to have a date past the current date.
 *
 * @param array $element
 *   An array of the element info to validate.
 * @param array &$form_state
 *   An array with the state of the form of the element.
 *
 * @see tmgmt_xplanation.ui.inc::checkoutSettingsForm()
 */
function tmgmt_xplanation_after_current_date(array $element, array &$form_state) {
  if ($element['#type'] == 'date_select') {
    $date_str = $element['#value']['year'] . '-';
    $date_str .= $element['#value']['month'] . '-';
    $date_str .= $element['#value']['day'] . ' ';
    $date_str .= $element['#value']['hour'] . ':';
    $date_str .= $element['#value']['minute'];
    $date = date_create($date_str, timezone_open($element['#date_timezone']));
    if ($date < new DateTime('now')) {
      $validator_err_msg = 'The %title needs to be after the current date!';
      $validator_err_msg_arr = array('%title' => $element['#title']);
      form_error($element, filter_xss(t($validator_err_msg, $validator_err_msg_arr)));
    }
  }
}

/**
 * Implements hook_form_ID_alter().
 *
 * Adds a link to manually check on the translation progress of the job.
 * The links is only displayed for jobs that use this module as the translator.
 */
function tmgmt_xplanation_form_tmgmt_job_form_alter(&$form, &$form_state, $form_id) {
  preg_match('/\d+/', current_path(), $matches);
  if (!is_null($matches) and count($matches) == 1) {
    $job = tmgmt_job_load($matches[0]);
    $active_job = $job->state == TMGMT_JOB_STATE_ACTIVE;
    if ($job->translator == 'xplanation' && $active_job) {
      $form['check_progress'] = array(
        '#type' => 'link',
        '#title' => t('Update Xplanation Progress'),
        '#href' => 'admin/check_xplanation_translation/' . $matches[0],
        '#weight' => 20,
      );
    }
  }

}

/**
 * Implements hook_form_ID_alter().
 *
 * Adds a link to manually check on the translation progress of all the jobs.
 */
function tmgmt_xplanation_form_views_form_tmgmt_ui_job_overview_page_alter(&$form, &$form_state) {
  $form['check_progress'] = array(
    '#type' => 'link',
    '#title' => t('Update Xplanation Progress'),
    '#href' => 'admin/check_xplanation_translations',
  );
}

/**
 * Implements hook_menu().
 *
 * Registers 2 urls with a callbackfunction:
 * - check_xplanation_translation/%; tmgmt_xplanation_check_translation($jobid)
 * - check_xplanation_translations; tmgmt_xplanation_check_all_translations_url
 */
function tmgmt_xplanation_menu() {
  return array(
    'admin/check_xplanation_translation/%' => array(
      'title' => 'Xplanation translator Callback 1 job',
      'description' => 'xplanation menu callback 1 job',
      'page callback' => 'tmgmt_xplanation_check_translation',
      'page arguments' => array(1),
      'access arguments' => array('administer tmgmt'),
      'type' => MENU_CALLBACK,
    ),
    'admin/check_xplanation_translations' => array(
      'title' => 'Xplanation translator Callback all jobs',
      'description' => 'xplanation menu callback all jobs',
      'page callback' => 'tmgmt_xplanation_check_all_translations_url',
      'access arguments' => array('administer tmgmt'),
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Implements hook_cron().
 *
 * Updates the progress of the active tmgmt jobs with this module
 * assigned as the translator. Only when 12 hours has passed after the
 * previous call will the function be executed.
 *
 * @see tmgmt_xplanation_check_all_translations()
 */
function tmgmt_xplanation_cron() {
  // 12 hours.
  $interval = 60 * 60 * 12;
  if (time() >= variable_get('tmgmt_xplanation_next_execution', 0)) {
    watchdog('tmgmt_xplanation', 'cron check status', array(), WATCHDOG_INFO);
    tmgmt_xplanation_check_all_translations();
    variable_set('tmgmt_xplanation_next_execution', time() + $interval);
  }
}

/**
 * Updates the progress of the active tmgmt jobs.
 *
 * Only job with this module assigned as the translator. Only when 12 hours has
 * passed after the previous call will the function be executed.
 */
function tmgmt_xplanation_check_all_translations_url() {
  tmgmt_xplanation_check_all_translations(TRUE);
}

/**
 * Updates the progress of the active tmgmt jobs.
 *
 * Only for jobs with this module assigned as the translator and when 12 hours
 * have passed after the previous call will the function be executed.
 *
 * @param bool $batch
 *   States if the function is running as a batch operation or not.
 */
function tmgmt_xplanation_check_all_translations($batch = FALSE) {
  $batch_err_msg = 'A problem occurred whilst checking the status of a job. ';
  $batch_err_msg .= 'Please try again later. ';
  $batch_err_msg .= 'If the problem persists contact ';
  $batch_err_msg .= TMGMT_XPLANATION_SUPPORT;
  $batch_definition = array(
    'operations' => array(),
    'title' => t('Xplanation job status check progress'),
    'progress_message' => t('@remaining more job(s) left to check of @total'),
    'error_message' => t($batch_err_msg),
    'finished' => 'tmgmt_xplanation_batch_finished',
  );
  try {
    // Load all the active jobs with this module as the translator.
    foreach (entity_load_multiple_by_name('tmgmt_job', FALSE) as $job) {
      $active_job = $job->state == TMGMT_JOB_STATE_ACTIVE;
      if ($job->translator == 'xplanation' && $active_job) {
        if ($batch) {
          $batch_operation = array('tmgmt_xplanation_check_translations_job_batch', array($job));
          array_push($batch_definition['operations'], $batch_operation);
        }
        else {
          tmgmt_xplanation_check_translations_job($job);
        }
      }
    }
  }
  catch (SoapFault $e) {
    $error = $e->faultstring . ' (CODE:' . $e->faultcode . ')';
    $log_err_msg = 'A problem occurred whilst checking ';
    $log_err_msg .= 'the status of a job. Message error: %e';
    $log_err_arr = array('%e' => $error);
    watchdog('tmgmt_xplanation', $log_err_msg, $log_err_arr, WATCHDOG_ERROR);
    $job_msg = 'A problem occurred whilst checking the status of a job. ';
    $job_msg .= 'Please try again later. If the problem persists contact ';
    $job_msg .= TMGMT_XPLANATION_SUPPORT;
    $job->addMessage(t($job_msg), array(), 'error');
  }
  if ($batch) {
    batch_set($batch_definition);
    batch_process('admin/tmgmt/jobs');
  }
}

/**
 * Implements hook_admin_paths().
 */
function tmgmt_xplanation_admin_paths() {
  return array('admin/check_xplanation_translations' => TRUE);
}

/**
 * Implements callback_batch_finished().
 *
 * Displays messages according to the result and inserts a log record
 * in case of an error.
 */
function tmgmt_xplanation_batch_finished($success, $results, $operations) {
  if (!isset($results['error'])) {
    $drupal_msg = 'The status is updated of all the jobs ';
    $drupal_msg .= 'using the Xplanation service';
    drupal_set_message(filter_xss(t($drupal_msg)));
  }
  else {
    reset($operations);
    $error_faultstring = $results['error']->faultstring;
    $error_faultcode = $results['error']->faultcode;
    $error = $error_faultstring . ' (CODE:' . $error_faultcode . ')';
    $drupal_msg = 'A problem occurred whilst checking the status of a ';
    $drupal_msg .= 'job. Please try again later. If the problem persists';
    $drupal_msg .= ' contact ';
    $drupal_msg .= TMGMT_XPLANATION_SUPPORT;
    drupal_set_message(filter_xss(t($drupal_msg), 'error'));
    $log_err_msg = 'A problem occurred whilst checking the status of ';
    $log_err_msg .= 'a job. Message error: %e';
    $log_err_arr = array('%e' => $error);
    watchdog('tmgmt_xplanation', $log_err_msg, $log_err_arr, WATCHDOG_ERROR);
  }
}

/**
 * Updates the progress status of a specific job.
 *
 * @param int $jobid
 *   The number of the tmgmt job of which the progress has to be updated.
 */
function tmgmt_xplanation_check_translation($jobid) {
  $job = tmgmt_job_load($jobid);
  try {
    if ($job == NULL) {
      drupal_set_message(t('Requested job does not exist'), 'error');
    }
    elseif ($job->translator == 'xplanation') {
      $active_job = $job->state == TMGMT_JOB_STATE_ACTIVE;
      if ($active_job) {
        tmgmt_xplanation_check_translations_job($job);
        drupal_set_message(t('The status is updated of the job'));
      }
      else {
        drupal_set_message(t('Job is already finished'), 'error');
      }
    }
  }
  catch (SoapFault $e) {
    $error = $e->faultstring . ' (CODE:' . $e->faultcode . ')';
    $log_err_msg = 'A problem occurred whilst checking the status of a ';
    $log_err_msg .= 'job. Message error: %e';
    $log_err_arr = array('%e' => $error);
    watchdog('tmgmt_xplanation', $log_err_msg, $log_err_arr, WATCHDOG_ERROR);
    $job_msg = 'A problem occurred whilst checking the status of a job. ';
    $job_msg .= 'Please try again later. If the problem persists ';
    $job_msg .= 'contact ';
    $job_msg .= TMGMT_XPLANATION_SUPPORT;
    $job->addMessage(t($job_msg), array(), 'error');
  }
  drupal_goto('admin/tmgmt/jobs/' . $jobid);
}

/**
 * Implements callback_batch_operation().
 *
 * Updates the progress status of a specific job.
 */
function tmgmt_xplanation_check_translations_job_batch(TMGMTJob $job, &$context) {
  if (!isset($context['results']['stopped'])) {
    try {
      tmgmt_xplanation_check_translations_job($job);
    }
    catch (SoapFault $e) {
      $current_set = &_batch_current_set();
      $current_set['success'] = FALSE;
      $context['results']['error'] = $e;
      $context['results']['stopped'] = TRUE;
      $context['finished'] = 1;
    }
  }
}

/**
 * Updates the progress status of a specific job.
 *
 * @param TMGMTJob $job
 *   The tmgmt job of which the progress has to be updated.
 */
function tmgmt_xplanation_check_translations_job(TMGMTJob $job) {
  $tmgmt_xplanation = $job->getTranslator()->getController();
  $tmgmt_xplanation->retrieveTranslation($job);
}

/**
 * Changes Job status to Active.
 *
 * If job status is finish, it possible to changes it back to Active, so a
 * updated translation can be used instead.
 */
function tmgmt_xplanation_reactivate_form_submit($form, &$form_state) {
  $job = $form_state['tmgmt_job'];
  if ($job->isFinished()) {
    $job->setState(TMGMT_JOB_STATE_ACTIVE, 'Re-Activated');
    foreach ($job->getItems() as $item) {
      $item->setState(TMGMT_JOB_ITEM_STATE_REVIEW);
    }
  }
}

/**
 * Import form submit callback.
 */
function tmgmt_xplanation_file_import_form_submit($form, &$form_state) {
  // Ensure we have the file uploaded.
  $job = $form_state['tmgmt_job'];

  $tmgmt_xplanation = $job->getTranslator()->getController();

  $file = file_save_upload('file', array('file_validate_extensions' => array('xml')), FALSE, FILE_EXISTS_RENAME);
  if ($file) {

    // Validate the file.
    $validated_job = $tmgmt_xplanation->validateImportFile($file->uri);
    if (!$validated_job) {
      $job->addMessage('Failed to validate manual uploaded file, import aborted.', array(), 'error');
    }
    elseif ($validated_job->tjid != $job->tjid) {
      $uri = $validated_job->uri();
      $label = $validated_job->label();
      $job->addMessage('Error: Import file is from another job <a href="@url">@label</a>, import aborted!', array('@url' => url($uri['path']), '@label' => $label));
    }
    else {
      try {
        // Validation successful, start import.
        // Load the xml file into a string.
        $xml_string = file_get_contents($file->uri);
        foreach ($job->getItems() as $item) {
          // Changes the status to need review.
          $item->setState(TMGMT_JOB_ITEM_STATE_REVIEW);
        }
        $new_data = $tmgmt_xplanation->parseTranslationData($xml_string);
        $job->addTranslatedData($new_data);
        $job->addMessage('Successfully manual imported file.');

        $job->setState(TMGMT_JOB_STATE_FINISHED, 'Project completed');

      }
      catch (Exception $e) {
        $job->addMessage('File import failed with the following message: @message', array('@message' => $e->getMessage()), 'error');
      }
    }
  }

  foreach ($job->getMessagesSince() as $message) {
    // Ignore debug messages.
    if ($message->type == 'debug') {
      continue;
    }
    $text = $message->getMessage();
    if ($text) {
      drupal_set_message(filter_xss($text), $message->type);
    }
  }
}

/**
 * Callback used to force re-sending of all items in a job.
 *
 * This is triggered from the UI checkoutInfo form.
 */
function tmgmt_xplanation_resubmit_translations($form, &$form_state) {
  drupal_set_message(t('Not possible to resubmit'), 'error');
}

/**
 * Add a messages when a job is deleted.
 */
function tmgmt_xplanation_job_delete(TMGMTJob $job) {
  drupal_set_message(t('The project is not deleted at Xplanation!'), 'error');
}
