INTRODUCTION
------------
TMGMT Translator Xplanation is a module created by Xplanation to enable you to 
handle your translations inside your known Drupal installation.

The TMGMT Translator Xplanation module is a simple and easy-to-install module 
built for Drupal 7. The module extends the Drupal 7 Translation management 
module so you can translate your website using our service. 
The module will regularly check for completed translations and make them 
available for review. TMGMT Translator Xplanation automates the entire 
translation process and makes it possible to have your site localised with 
just a few simple clicks.

Consult the User Guide at 
http://www.xplanation.com/pdfs/TMGMT_Translator_Xplanation_User_Guide_EN.pdf
which describes, step by step, how the module can be installed, translations 
can be registered, followed up and imported.

REQUIREMENTS
------------
This module requires the following modules:
  * Translation management tool (https://drupal.org/project/tmgmt)
  * Date (https://drupal.org/project/date)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Contact us as jef.verelst@xplanation.com to receive a configuration file.
   The file can be imported via Administration » Configuration » TMGMT » 
   Regional and Language » Translation Management Translators » 
   Import Translator

TROUBLESHOOTING
---------------
Links included in the text should be manually updated after translation.
A <a href="/node/2216373">visual review</a> can not provided. This means that
the received translations can not generate a view of the website based on
these translations before they are accepted. Partly because the review process
is provided by the tmgmt module and because this can not be implemented until
a bug with the text-formats (Issue: 1215234) is fixed.

MAINTAINERS
-----------
Current maintainers:
 * Mathias Ver Elst - https://drupal.org/user/1276984
 * Mikkel Madsen - https://drupal.org/user/

This project has been sponsored by:
 * Xplanation
 * JYSK
