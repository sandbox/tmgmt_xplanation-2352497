<?php
/**
 * @file
 * Contains the controller information to connect to the Xplanation webservices.
 *
 * The webservices and the data-format are constructed such that future changes
 * can be made to the format whilest staying backwards compatible.
 */

/**
 * Controller class for the Xplanation jobs.
 */
class TMGMTXplanationPluginController extends TMGMTDefaultTranslatorPluginController {
  // A list of the supported languages.
  // Note that the actual mapping is done via the tmgmt provided methods.
  // Languages in comments can always be provided but are currently not
  // recognised by the webservice.
  // Please contact support@xplanation.com for more information.
  protected $supportedLanguages = array(
    // 'ab' => null,.
    // Abkhazian.
    // 'aa' => null,.
    // Afar.
    'af' => array(
      'af' => 'Afrikaans',
    ),
    // Afrikaans.
    // 'ak' => null,.
    // Akan.
    'sq' => array(
      'sq' => 'Albanian',
    ),
    // Albanian Shqip.
    'am' => array(
      'am' => 'Amharic',
    ),
    // Amharic አማርኛ.
    'ar' => array(
      'ar' => 'Arabic',
      'ar-MA' => 'Arabic (Morocco)',
      'ar-TN' => 'Arabic (Tunisia)'),
    // Arabic العربية.
    'hy' => array(
      'hy' => 'Armenian',
    ),
    // Armenian Հայերեն.
    // 'as' => null,.
    // Assamese.
    // 'ast' => null,.
    // Asturian.
    // 'av' => null,.
    // Avar.
    // 'ae' => null,.
    // Avestan.
    // 'ay' => null,.
    // Aymara.
    'az' => array(
      'az' => 'Azerbaijani',
    ),
    // Azerbaijani azərbaycan.
    // 'bm' => null,.
    // Bambara Bamanankan.
    // 'ba' => null,.
    // Bashkir.
    'eu' => array(
      'eu' => 'Basque',
    ),
    // Basque Euskera.
    'be' => array(
      'be' => 'Belarusian',
    ),
    // Belarusian Беларуская.
    'bn' => array(
      'bn', 'Bengali',
    ),
    // Bengali.
    // 'dz' => null,.
    // Bhutani.
    // 'bh' => null,.
    // Bihari.
    // 'bi' => null,.
    // Bislama.
    'bs' => array(
      'bs' => 'Bosnian',
    ),
    // Bosnian Bosanski.
    // 'br' => null,.
    // Breton.
    'bg' => array(
      'bg' => 'Bulgarian',
    ),
    // Bulgarian Български.
    'my' => array(
      'my' => 'Burmese',
    ),
    // Burmese.
    // 'km' => null,.
    // Cambodian.
    'ca' => array(
      'ca' => 'Catalan',
    ),
    // Catalan Català.
    // 'ch' => null,.
    // Chamorro.
    // 'ce' => null,.
    // Chechen.
    // 'ny' => null,.
    // Chichewa.
    // Chinese, Simplified 简体中文.
    'zh-hans' => array(
      'zh-Hans' => 'Chinese (Simplified)',
    ),
    // Chinese, Traditional 繁體中文.
    'zh-hant' => array(
      'zh-Hant-HK' => 'Chinese (Traditional for Hong Kong)',
      'zh-Hant-TW' => 'Chinese (Traditional for Taiwan)',
    ),
    // 'cv' => null,.
    // Chuvash.
    // 'kw' => null,.
    // Cornish.
    // 'co' => null,.
    // Corsican.
    // 'cr' => null,.
    // Cree.
    'hr' => array(
      'hr' => 'Croatian',
    ),
    // Croatian Hrvatski.
    'cs' => array(
      'cs' => 'Czech',
    ),
    // Czech Čeština.
    'da' => array(
      'da' => 'Danish',
    ),
    // Danish Dansk.
    'nl' => array(
      'nl' => 'Dutch (The Netherlands)',
      'nl-BE' => 'Dutch (Belgium)',
    ),
    // Dutch Nederlands.
    'en-us' => array(
      'en-US' => 'English (US)',
    ),
    // English.
    'en-gb' => array(
      'en-GB' => 'English (US)',
    ),
    // English.
    'en' => array(
      'en-GB' => 'English (UK)',
    ),
    // English, British.
    // 'eo' => null,.
    // Esperanto.
    'et' => array(
      'et' => 'Estonian',
    ),
    // Estonian Eesti.
    // 'ee' => null,.
    // Ewe Ɛʋɛ.
    'fo' => array(
      'fo' => 'Faroese',
    ),
    // Faeroese.
    // 'fj' => null,.
    // Fiji.
    // 'fil' => null,.
    // Filipino.
    'fi' => array(
      'fi' => 'Finnish',
    ),
    // Finnish Suomi.
    'fr' => array(
      'fr' => 'French',
      'fr-BE' => 'French (Belgium)',
      'fr-CA' => 'French (Canada)',
      'fr-CH' => 'French (Switzerland)',
    ),
    // French Français.
    // 'fy' => null,.
    // Frisian Frysk.
    // 'ff' => null,.
    // Fulah Fulfulde.
    'gl' => array(
      'gl' => 'Galician',
    ),
    // Galician Galego.
    'ka' => array(
      'ka' => 'Georgian',
    ),
    // Georgian.
    'de' => array(
      'de' => 'German',
      'de-AT' => 'German (Austria)',
      'de-CH' => 'German (Switzerland)',
    ),
    // German Deutsch.
    'el' => array(
      'el' => 'Greek',
    ),
    // Greek Ελληνικά.
    // 'kl' => null,.
    // Greenlandic.
    // 'gn' => null,.
    // Guarani.
    // 'gu' => null,.
    // Gujarati.
    // 'ht' => null,.
    // Haitian Creole.
    'ha' => array(
      'ha' => 'Hausa',
    ),
    // Hausa.
    'he' => array(
      'he' => 'Hebrew',
    ),
    // Hebrew עברית.
    // 'hz' => null,.
    // Herero.
    'hi' => array(
      'hi' => 'Hindi',
    ),
    // Hindi हिन्दी.
    // 'ho' => null,.
    // Hiri Motu.
    'hu' => array(
      'hu' => 'Hungarian',
    ),
    // Hungarian Magyar.
    'is' => array(
      'is' => 'Icelandic',
    ),
    // Icelandic Íslenska.
    'ig' => array(
      'ig' => 'Igbo',
    ),
    // Igbo.
    'id' => array(
      'id' => 'Indonesian',
    ),
    // Indonesian Bahasa Indonesia.
    // 'ia' => null,.
    // Interlingua.
    // 'ie' => null,.
    // Interlingue.
    // 'iu' => null,.
    // Inuktitut.
    // 'ik' => null,.
    // Inupiak.
    'ga' => array(
      'ga' => 'Irish',
    ),
    // Irish Gaeilge.
    'it' => array(
      'it' => 'Italian',
      'it-CH' => 'Italian (Switzerland)',
    ),
    // Italian Italiano.
    'ja' => array(
      'ja' => 'Japanese',
    ),
    // Japanese 日本語.
    // 'jv' => null,.
    // Javanese.
    // 'kn' => null,.
    // Kannada ಕನ್ನಡ.
    // 'kr' => null,.
    // Kanuri.
    // 'ks' => null,.
    // Kashmiri.
    'kk' => array(
      'kk' => 'Kazakh',
    ),
    // Kazakh Қазақ.
    // 'ki' => null,.
    // Kikuyu.
    // 'rw' => null,.
    // Kinyarwanda.
    'rn' => array(
      'rn' => 'Kirundi',
    ),
    // Kirundi.
    // 'kv' => null,.
    // Komi.
    // 'kg' => null,.
    // Kongo.
    'ko' => array(
      'ko' => 'Korean',
    ),
    // Korean 한국어.
    'ku' => array(
      'ku' => 'Kurdisch',
    ),
    // Kurdish Kurdî.
    // 'kj' => null,.
    // Kwanyama.
    // 'ky' => null,.
    // Kyrgyz Кыргызча.
    'lo' => array(
      'lo' => 'Lao',
    ),
    // Laothian.
    // 'la' => null,.
    // Latin Latina.
    'lv' => array(
      'lv' => 'Latvian',
    ),
    // Latvian Latviešu.
    // 'ln' => null,.
    // Lingala.
    'lt' => array(
      'lt' => 'Lithuanian',
    ),
    // Lithuanian Lietuvių.
    // 'lg' => null,.
    // Luganda.
    'lb' => array(
      'lb' => 'Luxembourgish',
    ),
    // Luxembourgish.
    'mk' => array(
      'mk' => 'Macedonian',
    ),
    // Macedonian Македонски.
    // 'mg' => null,.
    // Malagasy.
    'ms' => array(
      'ms' => 'Malay',
    ),
    // Malay Bahasa Melayu.
    // 'ml' => null,.
    // Malayalam മലയാളം.
    // 'dv' => null,.
    // Maldivian.
    'mt' => array(
      'mt' => 'Maltese',
    ),
    // Maltese Malti.
    // 'gv' => null,.
    // Manx.
    // 'mr' => null,.
    // Marathi.
    // 'mh' => null,.
    // Marshallese.
    // 'mo' => null,.
    // Moldavian.
    // 'mn' => null,.
    // Mongolian.
    // 'mi' => null,.
    // Māori.
    // 'na' => null,.
    // Nauru.
    // 'nv' => null,.
    // Navajo.
    // 'ng' => null,.
    // Ndonga.
    // 'ne' => null,.
    // Nepali.
    'nd' => array(
      'nr' => 'Ndebele',
    ),
    // North Ndebele.
    // 'se' => null,.
    // Northern Sami.
    'nb' => array(
      'no' => 'Norwegian',
    ),
    // Norwegian Bokmål Bokmål.
    'nn' => array(
      'no' => 'Norwegian',
    ),
    // Norwegian Nynorsk Nynorsk.
    'oc' => array(
      'oc' => 'Occitan',
    ),
    // Occitan.
    // 'cu' => null,.
    // Old Slavonic.
    // 'or' => null,.
    // Oriya.
    // 'om' => null,.
    // Oromo.
    // 'os' => null,.
    // Ossetian.
    // 'pi' => null,.
    // Pali.
    'ps' => array(
      'ps' => 'Pashto',
    ),
    // Pashto پښتو.
    'fa' => array(
      'fa' => 'Farsi',
    ),
    // Persian فارسی.
    'pl' => array(
      'pl' => 'Polish',
    ),
    // Polish Polski.
    // Portuguese, Brazil Português.
    'pt-br' => array(
      'pt-BR' => 'Portuguese (Brazil)',
    ),
    // Portuguese, International.
    'pt' => array(
      'pt' => 'Portuguese (Portugal)',
    ),
    // Portuguese, Portugal Português.
    'pt-pt' => array(
      'pt' => 'Portuguese (Portugal)',
    ),
    // 'pa' => null,.
    // Punjabi.
    // 'qu' => null,.
    // Quechua.
    'rm' => array(
      'rm' => 'Romansh',
    ),
    // Rhaeto-Romance.
    'ro' => array(
      'ro' => 'Romanian',
    ),
    // Romanian Română.
    'ru' => array(
      'ru' => 'Russian',
    ),
    // Russian Русский.
    // 'sm' => null,.
    // Samoan.
    // 'sg' => null,.
    // Sango.
    // 'sa' => null,.
    // Sanskrit.
    // 'sc' => null,.
    // Sardinian.
    // 'sco' => null,.
    // Scots.
    // 'gd' => null,.
    // Scots Gaelic.
    'sr' => array(
      'sr' => 'Serbian (Latin)',
    ),
    // Serbian Српски.
    'sh' => array(
      'sh' => 'Serbo-Croatian',
    ),
    // Serbo-Croatian.
    // 'st' => null,.
    // Sesotho.
    'tn' => array(
      'tn' => 'Tswana',
    ),
    // Setswana.
    // 'sn' => null,.
    // Shona.
    // 'sd' => null,.
    // Sindhi.
    'si' => array(
      'si-LK' => 'Sinhala',
    ),
    // Sinhala සිංහල.
    'ss' => array(
      'ss' => 'Swati',
    ),
    // Siswati.
    'sk' => array(
      'sk' => 'Slovak',
    ),
    // Slovak Slovenčina.
    'sl' => array(
      'sl' => 'Slovenian',
    ),
    // Slovenian Slovenščina.
    'so' => array(
      'so' => 'Somali',
    ),
    // Somali.
    'nr' => array(
      'nr' => 'Ndebele',
    ),
    // South Ndebele.
    'es' => array(
      'es' => 'Spanish (Spain)',
      'es-419' => 'Spanish (Latin America)',
    ),
    // Spanish Español.
    // 'su' => null,.
    // Sudanese.
    'sw' => array(
      'sw' => 'Swahili',
    ),
    // Swahili Kiswahili.
    'sv' => array(
      'sv' => 'Swedish',
      'sv-FI' => 'Swedish (Finland)',
    ),
    // Swedish Svenska.
    'gsw-berne' => array(
      'de-CH' => 'German (Switzerland)',
    ),
    // Swiss German.
    'tl' => array(
      'tl' => 'Tagalog',
    ),
    // Tagalog.
    // 'ty' => null,.
    // Tahitian.
    // 'tg' => null,.
    // Tajik.
    'ta' => array(
      'ta' => 'Tamil',
    ),
    // Tamil தமிழ்.
    // 'tt' => null,.
    // Tatar Tatarça.
    // 'te' => null,.
    // Telugu తెలుగు.
    'th' => array(
      'th' => 'Thai',
    ),
    // Thai ภาษาไทย.
    'bo' => array(
      'bo' => 'Tibetan',
    ),
    // Tibetan.
    'ti' => array(
      'ti' => 'Tigrinya',
    ),
    // Tigrinya.
    // 'to' => null,.
    // Tonga.
    'ts' => array(
      'ts' => 'Tsonga',
    ),
    // Tsonga.
    'tr' => array(
      'tr' => 'Turkish',
    ),
    // Turkish Türkçe.
    'tk' => array(
      'tk' => 'Turkmen',
    ),
    // Turkmen.
    // 'tw' => null,.
    // Twi.
    // 'ug' => null,.
    // Uighur.
    'uk' => array(
      'uk' => 'Ukranian',
    ),
    // Ukrainian Українська.
    'ur' => array(
      'ur' => 'Urdu',
    ),
    // Urdu اردو.
    'uz' => array(
      'uz' => 'Uzbek',
    ),
    // Uzbek o'zbek.
    've' => array(
      've' => 'Venda',
    ),
    // Venda.
    'vi' => array(
      'vi' => 'Vietnamese',
    ),
    // Vietnamese Tiếng Việt.
    'cy' => array(
      'cy' => 'Welsh',
    ),
    // Welsh Cymraeg.
    // 'wo' => null,.
    // Wolof.
    'xh' => array(
      'xh' => 'Xhosa',
    ),
    // Xhosa isiXhosa.
    // 'yi' => null,.
    // Yiddish.
    // 'yo' => null,.
    // Yoruba Yorùbá.
    // 'za' => null,.
    // Zhuang.
    'zu' => array(
      'zu' => 'Zulu',
    ),
    // Zulu isiZulu.
  );

  /**
   * Get options for soap requests.
   *
   * @param TMGMTTranslator $translator
   *   The translator object containing the settings.
   *
   * @return array
   *   An array containing the soap connection options.
   */
  public function getSoapOptions(TMGMTTranslator $translator) {
    $options = array(
      'trace' => TRUE,
      'connection_timeout' => 2000,
      'keep_alive' => FALSE,
      'encoding' => 'UTF-8',
    );
    $proxy_settings_set = FALSE;
    if (!is_null($translator->getSetting('proxy'))) {
      $proxy_options = $translator->getSetting('proxy');
      if (!is_null($proxy_options['host']) &&
        strlen($proxy_options['host']) > 0) {
        $proxy_settings_set = TRUE;
        $options['proxy_host'] = $proxy_options['host'];
      }
      if (!is_null($proxy_options['port']) &&
        strlen($proxy_options['port']) > 0) {
        $proxy_settings_set = TRUE;
        $options['proxy_port'] = $proxy_options['port'];
      }
      if (!is_null($proxy_options['login']) &&
        strlen($proxy_options['login']) > 0) {
        $proxy_settings_set = TRUE;
        $options['proxy_login'] = $proxy_options['login'];
      }
      if (!is_null($proxy_options['password']) &&
        strlen($proxy_options['password']) > 0) {
        $proxy_settings_set = TRUE;
        $options['proxy_password'] = $proxy_options['password'];
      }
    }
    if (!$proxy_settings_set) {
      if (!is_null(variable_get('tmgmt_xplanation_proxy_host', NULL)) &&
        strlen(variable_get('tmgmt_xplanation_proxy_host', NULL)) > 0) {
        $options['proxy_host'] = variable_get('tmgmt_xplanation_proxy_host', NULL);
      }
      if (!is_null(variable_get('tmgmt_xplanation_proxy_port', NULL)) &&
        strlen(variable_get('tmgmt_xplanation_proxy_port', NULL)) > 0) {
        $options['proxy_port'] = variable_get('tmgmt_xplanation_proxy_port', NULL);
      }
      if (!is_null(variable_get('tmgmt_xplanation_proxy_login', NULL)) &&
        strlen(variable_get('tmgmt_xplanation_proxy_login', NULL)) > 0) {
        $options['proxy_login'] = variable_get('tmgmt_xplanation_proxy_login', NULL);
      }
      if (!is_null(variable_get('tmgmt_xplanation_proxy_password', NULL)) &&
        strlen(variable_get('tmgmt_xplanation_proxy_password', NULL)) > 0) {
        $options['proxy_password'] = variable_get('tmgmt_xplanation_proxy_password', NULL);
      }
    }
    return $options;
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::isAvailable().
   */
  public function isAvailable(TMGMTTranslator $translator) {
    if (!class_exists('SoapClient')) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::getNotAvailableReason().
   */
  public function getNotAvailableReason(TMGMTTranslator $translator) {
    $items = array();
    if (!class_exists('SoapClient')) {
      $soap_client_msg = t('The PHP Soap module does not appear to be enabled in
      this web server configuration. For more information, please see the
      <a href="@soap-url">php.net soap article</a>', array(
        '@soap-url' => 'http://www.php.net/manual/en/soap.installation.php',
      ));
      return $soap_client_msg;
    }

    if (!$translator->getSetting('server')) {
      $items[] = 'Server address';
    }
    if (!$translator->getSetting('process_code')) {
      $items[] = 'Process code';
    }
    if (!$translator->getSetting('domainCode')) {
      $items[] = 'Domain Code';
    }
    if (!$translator->getSetting('username')) {
      $items[] = 'username';
    }
    if (!$translator->getSetting('password')) {
      $items[] = 'password';
    }
    return t('Configuration value needed for @items.',
      array('@items' => implode(', ', $items)));
  }

  /**
   * {@inheritdoc}
   */
  public function canTranslate(TMGMTTranslator $translator, TMGMTJob $job) {
    if ($this->isAvailable($translator)) {
      return TRUE;
    }
    return FALSE;
  }


  /**
   * {@inheritdoc}
   */
  public function requestTranslation(TMGMTJob $job) {
    $data = $this->prepareDataForSending($job);
    $translator = $job->getTranslator();
    $due_date = new DateTime('now');
    $due_date->add(new DateInterVal('P1D'));
    if (!is_null($job->getSetting('due_date'))) {
      $due_date = new DateTime($job->getSetting('due_date'));
    }
    variable_set('tmgmt_xplanation_due_date', $due_date->format('Y-m-d H:i'));
    $project_comments = 'Drupal project request';
    $user = $GLOBALS['user'];
    if ((!is_null($user->mail)) && (!is_null($user->name))) {
      $project_comments .= ' by ' . $user->name . ' (' . $user->mail . ')';
    }
    elseif (!is_null($user->mail)) {
      $project_comments .= ' by ' . $user->mail;
    }
    if (!is_null($job->getSetting('project_comments')) &&
      strlen($job->getSetting('project_comments') > 0)) {
      $project_comments .= '\n\n' . $job->getSetting('project_comments');
      variable_set('tmgmt_xplanation_comments', $job->getSetting('project_comments'));
    }
    $tstream_src_code = $translator->mapToRemoteLanguage($job->source_language);
    $tstream_trg_code = $translator->mapToRemoteLanguage($job->target_language);
    try {
      $ws_name = '/tstreamws/base64_projectlaunch?wsdl';
      $url = $translator->getSetting('server') . $ws_name;
      $client = new SoapClient($url, $this->getSoapOptions($translator));
      $project_title = $job->label . ' (Drupal ref: ' . $job->tjid . ')';
      $doc_type = 'Xplanation/drupal';
      $result = $client->__soapCall('launch', array(array(
        'processCode' => $translator->getSetting('process_code'),
        'processType' => $translator->getSetting('service'),
        'projectTitle' => $project_title,
        'projectNumber' => $job->tjid,
        'docTypeCode' => $doc_type,
        'domainCode' => $translator->getSetting('customer_domain_code'),
        'sourceLanguage' => $tstream_src_code,
        'targetLanguages' => array($tstream_trg_code),
        'dueDate' => $due_date->format('Y-m-d H:i:s'),
        'projectComment' => $project_comments,
        'fileName' => $job->tjid . '.xml',
        'base64File' => $data,
        'userLogin' => $translator->getSetting('username'),
        'password' => $translator->getSetting('password'),
        )));
      $job->reference = $result->return;
      $job_submitted_msg = 'Job has been successfully submitted for ';
      $job_submitted_msg .= 'translation. Remote ID is: W%waiting_room_id. ';
      $job_submitted_msg .= ' (';
      $job_submitted_msg .= tmgmt_language_label($job->source_language);
      $job_submitted_msg .= ' into ';
      $job_submitted_msg .= tmgmt_language_label($job->target_language);
      $job_submitted_msg .= ' )';
      $job_submitted_arr = array('%waiting_room_id' => $job->reference);
      $job->submitted($job_submitted_msg, $job_submitted_arr);
      $job->setState(TMGMT_JOB_STATE_ACTIVE);
    }
    catch (SoapFault $e) {
      $error = $e->faultstring . ' (CODE:' . $e->faultcode . ')';
      $watch_msg = 'A problem occurred whilst registering a job. ';
      $watch_msg .= 'Message error: %e';
      $watch_arr = array('%e' => $error);
      watchdog('tmgmt_xplanation', $watch_msg, $watch_arr, WATCHDOG_ERROR);
      $job_msg = 'A problem occurred whilst connecting with the Xplanation ';
      $job_msg .= 'webservice. Please try again later. If the problem ';
      $job_msg .= 'persists contact ' . TMGMT_XPLANATION_SUPPORT;
      $job->addMessage($job_msg, array(), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveTranslation(TMGMTJob $job) {
    $translator = $job->getTranslator();
    $ws_name = '/tstreamws/projectws?wsdl';
    $url = $translator->getSetting('server') . $ws_name;
    $client = new SoapClient($url, $this->getSoapOptions($translator));
    $result = $client->__soapCall('checkProjectStatus', array(
      array(
        'projectId' => $job->reference,
        'userLogin' => $translator->getSetting('username'),
        'password' => $translator->getSetting('password'),
      ),
    ));
    if ('COMP' == $result->return) {
      $watch_msg = 'retrieve translation job %job';
      $watch_arr = array('%job' => $job->tjid);
      watchdog('tmgmt_xplanation', $watch_msg, $watch_arr, WATCHDOG_INFO);
      $ws_name = '/tstreamws/base64_projectlaunch?wsdl';
      $url = $translator->getSetting('server') . $ws_name;
      $client = new SoapClient($url, $this->getSoapOptions($translator));
      $tstream_trg_code = $translator->mapToRemoteLanguage($job->target_language);
      $result = $client->__soapCall('retrieveDocument', array(
        array(
          'projectId' => $job->reference,
          'targetLanguageCode' => $tstream_trg_code,
          'userLogin' => $translator->getSetting('username'),
          'password' => $translator->getSetting('password'),
        ),
      ));
      $job->addTranslatedData($this->parseTranslationData($result->return));
      $job->setState(TMGMT_JOB_STATE_FINISHED, 'Project completed');
    }
    elseif ('CANC' == $result->return) {
      $job->setState(TMGMT_JOB_STATE_ABORTED, 'Project cancelled');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTargetLanguages(TMGMTTranslator $translator, $source_language) {
    return drupal_map_assoc(array_keys($this->supportedLanguages));
  }

  /**
   * Prepare the file-string that can be send to the Xplanation webservice.
   *
   * The file generated will look like this:
   * @code
   * <items website="http://www.xplanation.com"
   *     tmgmt-version="7.x-1.0-rc1"
   *     tmgmt-xplanation-version="7.x-1.0">
   *   <item key="87][node_title">
   *     <text type="unknown">&lt;i&gt;Example&lt;/i&gt; node title</text>
   *   </item>
   *   <item key="88][body][0][value">
   *     <text type="unknown">&lt;i&gt;Example&lt;/i&gt; node body</text>
   *   </item>
   * </items>
   * @endcode
   *
   * @todo Add type based on the text-format once issue
   * https://www.drupal.org/node/1415234 is resolved.
   * Probably a combination of the settings in the text-format that can
   * be passed to our converter
   *
   * @param TMGMTJob $job
   *   The TMGMTJob to written to the file.
   *
   * @return string
   *   A string representing the file ready to be translated.
   */
  protected function prepareDataForSending(TMGMTJob $job) {
    $data = array_filter(tmgmt_flatten_data($job->getData()), '_tmgmt_filter_data');
    $items = '';
    $item_str = '<item key="@key"><text type="unknown">@text</text></item>';
    foreach ($data as $key => $value) {
      $text = str_replace(array('&', '<'), array('&amp;', '&lt;'), $value['#text']);
      $items .= str_replace(array('@key', '@text'), array($key, $text), $item_str);
    }
    global $base_url;
    $info = system_get_info('module', 'tmgmt_xplanation');
    $tmgmt_info = system_get_info('module', 'tmgmt');
    $file = '<items website="' . $base_url . '" ';
    $file .= 'tmgmt-version="' . $tmgmt_info['version'] . '" ';
    $file .= 'sourceLanguage="' . $job->getTranslator()->mapToRemoteLanguage($job->source_language) . '" ';
    $file .= 'targetLanguage="' . $job->getTranslator()->mapToRemoteLanguage($job->target_language) . '" ';
    $file .= 'job-id="' . $job->tjid . '" ';
    $file .= 'tmgmt-xplanation-version="' . $info['version'] . '">';
    $file .= $items;
    $file .= '</items>';
    return $file;
  }

  /**
   * Parses the translated file string received from the Xplanation webservice.
   *
   * @param string $data
   *   The received file-string from the Xplanation webservice after a
   *   translation.
   *
   * @return array
   *   A nested array containing a TMGMTJob translations.
   *
   * @todo: Implement a check on the incoming data and parse based on the
   * module version.
   */
  public function parseTranslationData($data) {
    $items = simplexml_load_string($data);
    $data = array();
    foreach ($items->item as $item) {
      $key = (string) $item['key'];
      $char_from_arr = array('&lt;', '&gt;', '&amp;', '&quot;', '&apos;');
      $char_to_arr = array('<', '>', '&', "'", "'");
      $text = str_replace($char_from_arr, $char_to_arr, (string) $item->text);
      $data[$key]['#text'] = $text;
      $data[$key]['#origin'] = 'local';
    }
    return tmgmt_unflatten_data($data);
  }


  /**
   * Aborts a translation job.
   *
   * @param TMGMTJob $job
   *   The job that should have its translation aborted.
   *
   * @return bool
   *   TRUE if the job could be aborted, FALSE otherwise.
   */
  public function abortTranslation(TMGMTJob $job) {
    if ($job->isActive()) {
      drupal_set_message(t('Unable to cancel running project. 
      Please contact your Xplanation project manager.'), 'error');
      return FALSE;
    }
    else {
      return TRUE;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function delete($jobs, $transaction) {
    drupal_set_message(t('The project is NOT deleted at Xplanation!'), 'error');
    return FALSE;
  }

  /**
   * Validates imported Xplanation XML file.
   *
   * Checks:
   * - Job ID.
   * - Target ans source languages
   *
   * @return bool
   *   A boolan FALSE if the file validate failed. Or the job
   */
  public function validateImportFile($imported_file) {
    try {
      $xml_string = file_get_contents($imported_file);
      $xml = simplexml_load_string($xml_string);
    }
    catch (Exception $e) {
      drupal_set_message(t('XML parsing: wrong formatted xml file'), 'error');
      return FALSE;
    }
    if (!$xml) {
      drupal_set_message(t('XML parsing: wrong formatted xml file'), 'error');
      return FALSE;
    }
    // Check if the job can be loaded.
    $job = tmgmt_job_load((string) $xml['job-id']);
    if (!isset($xml['job-id']) || !$job) {
      drupal_set_message(t('XML parsing: Job-id value not found or wrong!'), 'error');
      return FALSE;
    }

    // Compare source language.
    if (!isset($xml['sourceLanguage']) ||
      $job->getTranslator()->mapToRemoteLanguage($job->source_language) != $xml['sourceLanguage']) {
      drupal_set_message(t('XML parsing: Source language value not found
      or wrong! @xml-source - @map-source', array(
        '@xml-source' => $xml['sourceLanguage'],
        '@map-source' => $job->getTranslator()->mapToRemoteLanguage($job->source_language),
      )), 'error');
      return FALSE;
    }

    // Compare target language.
    if (!isset($xml['targetLanguage']) ||
      $job->getTranslator()->mapToRemoteLanguage($job->target_language) != $xml['targetLanguage']) {
      drupal_set_message(t('XML parsing: Target language value not found
      or wrong!'), 'error');
      return FALSE;
    }

    // Validation successful.
    return $job;
  }

}
